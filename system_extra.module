<?php
/**
 * @file
 */

/**
 * Implements hook_flush_caches().
 */
function system_extra_flush_caches() {
  $execute = &drupal_static(__FUNCTION__, TRUE);

  if ($execute) {
    // Prevento to run it multiple times as module status changes produce cache clearing.
    $execute = FALSE;

    drupal_set_message(t('Going to enabled modules dependencies verification.'));

    $modules = module_list();

    // Remove installation profiles from modules list.
    foreach(array('standard', 'minimal') as $name) {
      if (($s_key = array_search($name, $modules)) !== false) {
        unset($modules[$s_key]);
      }
    }
    // Enabled modules dependencies verification.
    _system_extra_module_enable($modules);
  }

  return array();
}

/**
 * Get missing modules dependencies by modules list.
 */
function _system_extra_get_missing_modules_dependencies($module_list) {
  // Get all module data so we can find dependencies and sort.
  $module_data = system_rebuild_module_data();

  $error_modules = array();

  foreach ($module_list as $module) {
    if (!isset($module_data[$module])) {
      // This module is not found in the filesystem, abort.
      $error_modules[] = $module;
    }
  }

  return $error_modules;
}

/**
 * Get disabled modules dependencies.
 */
function _system_extra_get_disabled_modules_dependencies($module_list) {
  // Get all module data so we can find dependencies and sort.
  $module_data = system_rebuild_module_data();
  // Create an associative array with weights as values.
  $module_list = array_flip(array_values($module_list));

  while (list($module) = each($module_list)) {
    if (!isset($module_data[$module])) {
      // This module is not found in the filesystem, abort.
      return FALSE;
    }

    // Add dependencies to the list, with a placeholder weight.
    // The new modules will be processed as the while loop continues.
    foreach (array_keys($module_data[$module]->requires) as $dependency) {
      if (!isset($module_list[$dependency])) {
        /*drupal_set_message(t('Added dependency module @dependency as required by @module', array(
          '@dependency' => $dependency,
          '@module' => $module,
        )));*/
        $module_list[$dependency] = 0;
      }
    }
  }

  // Handle modules removal separatelly to prevent looping caused dependency loop between enabled modules.
  foreach (array_keys($module_list) as $module) {
    // Remove already enabled modules.
    if ($module_data[$module]->status) {
      // Skip already enabled modules.
      unset($module_list[$module]);
    }
  }

  return array_keys($module_list);
}

/**
 * Enable modules with dependencies.
 */
function _system_extra_module_enable($module_list) {
  // Get disabled modules only.
  $enable_modules = _system_extra_get_disabled_modules_dependencies($module_list);

  if ($enable_modules === FALSE) {
    $error_modules = _system_extra_get_missing_modules_dependencies($module_list);

    drupal_set_message(t('Can not enable modules as such modules are not available: @modules', array(
      '@modules' => implode(', ', $error_modules),
    )), 'error');
  }
  else {
    // Success.
    if ($enable_modules) {
      // Enabled modules.
      $msg_type = (module_enable($enable_modules)) ? 'status' : 'error';

      drupal_set_message(t('Enabled new modules and their dependencies: @modules', array(
        '@modules' => implode(', ', $enable_modules),
      )), $msg_type);
    }
    else {
      drupal_set_message(t('All modules and their dependencies are enabled already.'));
    }
  }

  return $enable_modules;
}

/**
 * Get disabled modules dependencies.
 */
function _system_extra_get_enabled_modules_dependants($module_list) {
  // Get all module data so we can find dependents and sort.
  $module_data = system_rebuild_module_data();
  // Create an associative array with weights as values.
  $module_list = array_flip(array_values($module_list));

  $profile = drupal_get_profile();
  while (list($module) = each($module_list)) {
    if (!isset($module_data[$module]) || !$module_data[$module]->status) {
      // This module doesn't exist or is already disabled, skip it.
      unset($module_list[$module]);
      continue;
    }
    $module_list[$module] = $module_data[$module]->sort;

    // Add dependent modules to the list, with a placeholder weight.
    // The new modules will be processed as the while loop continues.
    foreach ($module_data[$module]->required_by as $dependent => $dependent_data) {
      if (!isset($module_list[$dependent]) && $dependent != $profile) {
        /*drupal_set_message(t('Added dependent module @dependent as required by @module', array(
          '@dependent' => $dependent,
          '@module' => $module,
        )));*/
        $module_list[$dependent] = 0;
      }
    }
  }

  return array_keys($module_list);
}

/**
 * Disable modules with dependencies.
 */
function _system_extra_module_disable($module_list) {
  // Get disabled modules only.
  $disable_modules = _system_extra_get_enabled_modules_dependants($module_list);

  if ($disable_modules) {
    // Disable modules.
    module_disable($disable_modules);

    drupal_set_message(t('Disabled modules and their dependants: @modules', array(
      '@modules' => implode(', ', $disable_modules),
    )), 'status');

    if (drupal_uninstall_modules($disable_modules)) {
      drupal_set_message(t('All modules and their dependencies are un-installed successfully.'));
    }
  }
  else {
    drupal_set_message(t('All modules and their dependants are disabled already.'));
  }

  return $disable_modules;
}
